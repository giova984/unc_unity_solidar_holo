﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class TestCameraMove : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (isServer && isLocalPlayer)
        {
            var x = Input.GetAxis("Horizontal") * 0.1f;
            var z = Input.GetAxis("Vertical") * 0.1f;
            transform.Translate(x, 0, z);
        }
        
    }
}
