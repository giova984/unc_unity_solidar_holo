﻿using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using System.Linq;


public class SpeechManager : MonoBehaviour
{
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    void Awake()
    {
        if (!HoloHelper.isHololens())
            this.enabled = false;
    }

    // Use this for initialization
    void Start()
    {
        keywords.Add("Reset world", () =>
        {
            // Call the OnReset method on every descendant object.
            this.BroadcastMessage("OnReset");
        });

        keywords.Add("Drop Sphere", () =>
        {
            var focusObject = GazeGestureManager.Instance.FocusedObject;
            if (focusObject != null)
            {
                // Call the OnDrop method on just the focused object.
                focusObject.SendMessage("OnDrop");
            }
        });

        keywords.Add("Reference Calibration", () =>
        {
            if(!ReferenceCalibration.Instance.isCalibrating())
                ReferenceCalibration.Instance.SendMessage("OnStartReferenceCalibration");
        });

        keywords.Add("Projector Calibration", () =>
        {
            if (!ProjectorCalibration.Instance.isCalibrating())
                ProjectorCalibration.Instance.SendMessage("OnStartProjectorCalibration");
        });

        keywords.Add("Stop Calibration", () =>
        {
            if (ReferenceCalibration.Instance.isCalibrating())
                ReferenceCalibration.Instance.BroadcastMessage("OnStopReferenceCalibration");
            if (ProjectorCalibration.Instance.isCalibrating())
                ProjectorCalibration.Instance.BroadcastMessage("OnStopProjectorCalibration");
        });

        keywords.Add("Clear Calibration", () =>
        {
            ReferenceCalibration.Instance.BroadcastMessage("OnClearCalibration");
        });

        keywords.Add("Show Calibration", () =>
        {
            BroadcastMessage("OnShowCalibration");
        });

        keywords.Add("Hide Calibration", () =>
        {
            BroadcastMessage("OnHideCalibration");
        });

        keywords.Add("Show People", () =>
        {
            BroadcastMessage("OnShowTelepresence");
        });

        keywords.Add("Hide People", () =>
        {
            BroadcastMessage("OnHideTelepresence");
        });

        keywords.Add("Show Capture", () =>
        {
            //BroadcastMessage("OnShowCapture");
            if (SpatialMapping.Instance)
                SpatialMapping.Instance.DrawVisualMeshes = true;

            //BroadcastMessage("OnShowTelepresence");
        });

        keywords.Add("Hide Capture", () =>
        {
            //BroadcastMessage("OnHideCapture");
            if (SpatialMapping.Instance)
                SpatialMapping.Instance.DrawVisualMeshes = false;

            //BroadcastMessage("OnHideTelepresence");
        });

        keywords.Add("Quit Application", () =>
        {
            Application.Quit();
        });

        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
}
