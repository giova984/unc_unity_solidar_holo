﻿using UnityEngine;
using System.Collections;

public class TelepresenceObj : MonoBehaviour {

    public bool show = true;

	// Use this for initialization
	void Start () {
        UpdateTelepresence();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void UpdateTelepresence()
    {
        if(gameObject.GetComponent<MeshRenderer>())
            gameObject.GetComponent<MeshRenderer>().enabled = show;
        if (gameObject.GetComponent<PLYPlayer>())
            gameObject.GetComponent<PLYPlayer>().enabled = show;
    }
    void OnShowTelepresence()
    {
        show = true;
        UpdateTelepresence();
    }
    void OnHideTelepresence()
    {
        show = false;
        UpdateTelepresence();
    }
}
