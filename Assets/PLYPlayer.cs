﻿using UnityEngine;
using System.Text;
using System.Collections;
using System.IO;

[ExecuteInEditMode]
public class PLYPlayer : MonoBehaviour {

    public string folder = "Assets/Resources/capture/";
    public bool preload_all = false;
    public int fps = 30;
    public bool play = true;
    public bool loop = true;

    // Use this for initialization

    private int curr_frame = 0;
    private PLYMeshLoader loader;
    private string[] frames = new string[0];

    private GameObject[] frameObjs = new GameObject[0];
    int curr_show_frame = -1;
    private bool isLoading = true;

    private float elapsedTime = 0;
    private float lastTime = 0;

    void Start () {
        DirectoryInfo dir = new DirectoryInfo(System.IO.Path.Combine(Application.streamingAssetsPath, folder));
        //Debug.Log("Dir: " + dir.Name);
        if (!dir.Exists)
        {
            Debug.Log("Path not found: " + folder);
        }
        else
        {
            FileInfo[] fileInfo = dir.GetFiles("*.ply", SearchOption.TopDirectoryOnly);
            Debug.Log(dir.Name + ": " + fileInfo.Length + " frames found");

            frames = new string[fileInfo.Length];
            if (preload_all)
            {
                frameObjs = new GameObject[fileInfo.Length];
            }

            gameObject.SetActive(false);
            //loader = gameObject.AddComponent<PLYMeshLoader>() as PLYMeshLoader;
            loader = GetComponent<PLYMeshLoader>();
            loader.gameObject.SetActive(false);
            gameObject.SetActive(true);

            for (int fi = 0; fi < fileInfo.Length; ++fi)
            {
                //string resName = folder + fileInfo[fi].Name.Replace(".txt", "");
                string resName = Path.Combine(Application.streamingAssetsPath, folder + fileInfo[fi].Name);
                Debug.Log(resName);
                //frames[fi] = Resources.Load<TextAsset>(resName);
                //WWW data = new WWW(resName);
                //frames[fi] = data;
                frames[fi] = resName;
                //Debug.Log("Resource Loaded " + frames[fi]);
                //Debug.Log(frames[fi]);
                if (preload_all && Application.isPlaying)
                {
                    LoadFrame(fi);
                    frameObjs[fi].SetActive(false);
                }
            }

            if (frames.Length > 0)
            {
                loader.PLY_file = frames[0];
                loader.loadPLY(this.GetComponent<MeshFilter>());
                loader.gameObject.SetActive(true);
            }
            if (Application.isPlaying && preload_all)
            {
                this.GetComponent<MeshRenderer>().enabled = false;
            }else
            {
                this.GetComponent<MeshRenderer>().enabled = true;
            }
        }

        isLoading = false;

        lastTime = Time.realtimeSinceStartup;
    }

    void LoadFrame(int i)
    {
        if(i >= 0 && i < frames.Length)
        {
            //Debug.Log("LoadFrame " + i);
            loader.PLY_file = frames[i];

            if (preload_all)
            {
                //if (!frameObjs[i])
                {
                    frameObjs[i] = new GameObject("frame_" + i);
                    frameObjs[i].transform.parent = this.transform;
                    frameObjs[i].transform.localPosition = new Vector3(0, 0, 0);
                    frameObjs[i].transform.localRotation = Quaternion.Euler(0, 0, 0);
                    MeshFilter mf = frameObjs[i].AddComponent<MeshFilter>() as MeshFilter;
                    MeshRenderer mr = frameObjs[i].AddComponent<MeshRenderer>() as MeshRenderer;
                    //mr.material = Resources.Load<Material>("Materials/PointCloudMat");
                    mr.sharedMaterial = this.GetComponent<MeshRenderer>().material;
                }
                loader.loadPLY(frameObjs[i].GetComponent<MeshFilter>());

            }else
            {
                loader.loadPLY(this.GetComponent<MeshFilter>());
            }

            loader.gameObject.SetActive(true);
        }
        //yield return 0;
    }

    void ShowFrame(int i)
    {
        if (preload_all)
        {
            if (curr_show_frame >= 0 && frameObjs[curr_show_frame] != null)
            {
                frameObjs[curr_show_frame].SetActive(false);
            }
            if (i < frameObjs.Length && frameObjs[i] != null)
            {
                frameObjs[i].SetActive(true);
                curr_show_frame = i;
            }
        }
        else
        {
            LoadFrame(i);
        }
    }
	// Update is called once per frame
	void Update () {
        if (play)
        {
            elapsedTime += Time.realtimeSinceStartup - lastTime;
        }
        lastTime = Time.realtimeSinceStartup;

        //fps = 10;

        if (loader && play)
        {
            if (!isLoading)
            {
                ShowFrame(curr_frame);
                //++curr_frame;
                curr_frame = Mathf.CeilToInt(elapsedTime * (float)fps);
                //Debug.Log("currframe " + curr_frame + " elapsed " + elapsedTime);

                if(curr_frame >= frames.Length)
                {
                    curr_frame = 0;
                    elapsedTime = elapsedTime - frames.Length * 1.0f / (float)fps;

                    if (!loop) {
                        play = false;
                        elapsedTime = 0;
                    }

                }
            }
        }

        
	}

    public void Play()
    {
        play = true;
    }
    public void Stop()
    {
        play = false;
    }
    public void PlayBegin()
    {
        curr_frame = 0;
        Play();
    }
}
