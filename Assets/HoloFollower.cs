﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HoloFollower : NetworkBehaviour
{
    private Camera m_mainCamera;

    protected Camera mainCamera
    {
        get
        {
            if (m_mainCamera == null)
            {
                m_mainCamera = Camera.current;
            }
            return m_mainCamera;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if ((isServer) || isLocalPlayer)
        //    return;

        //if ((!isServer) || !isLocalPlayer)
        //    return;

        if (isServer) //Hololens
        {
            if (mainCamera)
            {
                //Camera camera = mainCamera.GetComponent<Camera>();
                this.transform.position = mainCamera.transform.position;
                this.transform.rotation = mainCamera.transform.rotation;
                this.transform.Translate(new Vector3(0.0f, 0.0f, 1.0f));
                //Debug.Log("Following main camera " + this.transform.position);
            }
            else
            {
                Debug.Log("Update NO main camera ");
            }
        }else //PC projector
        {
            if (mainCamera)
            {
                //Camera camera = mainCamera.GetComponent<Camera>();
                Camera camera = mainCamera;
                camera.transform.position = this.transform.position;
                camera.transform.rotation = this.transform.rotation;
                //camera.transform.Translate(new Vector3(0.1f, 0.1f, 0.1f));
                Debug.Log("Update main camera " + this.transform.position);
            }
            else
            {
                Debug.Log("Update NO main camera ");
            }
        }

        
    }
}