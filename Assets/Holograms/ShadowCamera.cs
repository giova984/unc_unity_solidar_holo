﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ShadowCamera : MonoBehaviour
{

    Camera projCamera = null;

    void UpdateCameraMatrices()
    {
        if (projCamera)
        {
            Matrix4x4[] projs = projCamera.GetStereoProjectionMatrices();
            Matrix4x4[] views = projCamera.GetStereoViewMatrices();
            Camera cam = this.GetComponent<Camera>();
            cam.SetStereoProjectionMatrices(projs[0], projs[1]);
            cam.SetStereoViewMatrices(views[0], views[1]);
            cam.worldToCameraMatrix = projCamera.worldToCameraMatrix;
            cam.projectionMatrix = projCamera.projectionMatrix;
        }
    }

    // Use this for initialization
    void Start()
    {
        if (transform.parent && transform.parent.GetComponent<Camera>())
        {
            projCamera = transform.parent.GetComponent<Camera>();
            //Debug.Log("projCamera found");
        }
        UpdateCameraMatrices();

    }

    // Update is called once per frame
    void Update()
    {
        UpdateCameraMatrices();
    }
}
