﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

[ExecuteInEditMode]
public class PLYMeshLoader : MonoBehaviour {
    public string PLY_file;
    [Range(0,100.0f)]
    public float scale = 0.01f;
    public bool renderAsPointCloud = false;
    private bool binary = false;

    private int ix, iy, iz, inx, iny, inz, ir, ig, ib, ia = -1;

    private bool loading = false;

    float loadingTime;

    //float[] floats = new float[50000 * 4];
    float[] floats = new float[0];
    Mesh mesh = null;

    // Use this for initialization
    void Start () {
        loadPLY();
    }
	
	// Update is called once per frame
	void Update () {
        //loadPLY();
	}



    //public struct vert
    //{
    //    public float _x, _y, _z;
    //    public float _nx, _ny, _nz;
    //    public char _r, _g, _b;
    //    public vert(float x, float y, float z, float nx, float ny, float nz, char r, char g, char b)
    //    {
    //        //_x = _y = _z = _nx = _ny = _nz  = 0;
    //        //_r = _g = _b = _a = (char)0;
    //        _x = x; _y = y; _z = z;
    //        _nx = nx; _ny = ny; _nz = nz;
    //        _r = r; _g = g; _b = b;
    //    }

    //    public string ToString()
    //    {
    //        return "[" + _x + "," + _y + "," + _z + "]" + " [" + _nx + "," + _ny + "," + _nz + "]" + " [" + (int)_r + "," + (int)_g + "," + (int)_b + "]";
    //    }
    //}

    public static float[] ConvertByteToFloat(byte[] array)
    {
        float[] floatArr = new float[array.Length / 4];
        for (int i = 0; i < floatArr.Length; i++)
        {
            if (BitConverter.IsLittleEndian)
            {
                //Array.Reverse(array, i * 4, 4);
            }
            floatArr[i] = BitConverter.ToSingle(array, i * 4);
        }
        return floatArr;
    }

    public void loadPLY()
    {
        MeshFilter meshf = GetComponent<MeshFilter>();
        loadPLY(meshf);
    }
    public void loadPLY(MeshFilter meshf)
    {
        loading = true;

        MeshFilter mf = meshf;
        if (mf.sharedMesh)
        {
#if UNITY_EDITOR
            DestroyImmediate(mf.sharedMesh);
#else
            Destroy(mf.sharedMesh);
#endif
        }
        mesh = new Mesh();
        mf.sharedMesh = mesh;

        //Debug.Log("Loading0 PLY ");// + (Time.realtimeSinceStartup).ToString());

        if (PLY_file == null)
        {
            return;
            //yield break;
        }

        //Debug.Log("Loading PLY " + PLY_file);// + (Time.realtimeSinceStartup).ToString());

        loadingTime = Time.realtimeSinceStartup;
        //yield return PLY_file;
        //while (!PLY_file.isDone)
        //    yield return new WaitForSeconds(0.1f);

        //Debug.Log("Loading PLY " + PLY_file);// + (Time.realtimeSinceStartup).ToString());
        //string resName = Path.Combine(Application.streamingAssetsPath, "capture/ply_500.txt");
        byte[] buffer = System.IO.File.ReadAllBytes(PLY_file);

        MemoryStream ms = new MemoryStream(buffer);
        StreamReader reader = new StreamReader(ms);

        string line;
        int count = 0;
        int vert_count = 0;
        //int header_count = 0;
        int tri_count = 0;
        bool end_header = false;
        bool end_body = false;

        Vector3[] vertices = new Vector3[1];
        int[] triangles = new int[1];
        Vector3[] normals = new Vector3[1];
        Color[] colors = new Color[1];
        int[] indices = new int[1];

        //List<vert> datavert = new List<vert>();

        bool element_vertex_header = false;
        bool element_faces_header = false;
        int property_num = 0;
        bool normals_present = false;
        bool colors_present = false;
        bool alpha_present = false;

        //TEST BINARY
        //end_header = true;

        long BytesRead = 0;

        while ((!end_header) && (line = reader.ReadLine()) != null){
            //Debug.Log("R: " + line);
            //++count;
            BytesRead += line.Length + 1;
            
            
            if (!end_header)
            {
                if (line.StartsWith("format")) {
                    line = line.Remove(0, "format ".Length);
                    if (line.StartsWith("binary_little_endian 1.0"))
                        binary = true;
                    else if (line.StartsWith("ascii 1.0"))
                        binary = false;
                }
                else if (line.StartsWith("end_header"))
                {
                    element_vertex_header = false;
                    element_faces_header = false;
                    property_num = 0;
                    end_header = true;
                    //header_count = count;
                } else if (line.StartsWith("element vertex "))
                {
                    element_vertex_header = true;
                    element_faces_header = false;
                    property_num = 0;

                    line = line.Remove(0, "element vertex ".Length);
                    vert_count = int.Parse(line);

                    //vert_count = Math.Min(vert_count, 10000); //DEBUG Hololens power

                    //Debug.Log("vertex count " + vert_count);
                    //datavert = new List<vert>(vert_count);
                    vertices = new Vector3[vert_count];
                    normals = new Vector3[vert_count];
                    colors = new Color[vert_count];
                    indices = new int[vert_count];
                }
                else if (line.StartsWith("element face "))
                {
                    element_vertex_header = false;
                    element_faces_header = true;
                    property_num = 0;

                    tri_count = int.Parse(line.Remove(0, "element face ".Length));
                    //Debug.Log("faces count " + tri_count);
                    triangles = new int[3 * tri_count];
                }else
                {
                    if (element_vertex_header)
                    {
                        
                        if (line.StartsWith("property"))
                        {
                            string[] tokens = line.Split();
                            string prop = tokens[tokens.Length - 1];
                            switch (prop)
                            {
                                case "x":
                                    ix = property_num;
                                    break;
                                case "y":
                                    iy = property_num;
                                    break;
                                case "z":
                                    iz = property_num;
                                    break;
                                case "nx":
                                    inx = property_num;
                                    normals_present = true;
                                    break;
                                case "ny":
                                    iny = property_num;
                                    break;
                                case "nz":
                                    inz = property_num;
                                    break;
                                case "red":
                                    ir = property_num;
                                    colors_present = true;
                                    break;
                                case "green":
                                    ig = property_num;
                                    break;
                                case "blue":
                                    ib = property_num;
                                    break;
                                case "alpha":
                                    ia = property_num;
                                    alpha_present = true;
                                    break;
                            }
                        }
                        ++property_num;
                    }else if (element_faces_header)
                    {

                        ++property_num;
                    }
                }
            }
        }
        

        while ((!end_body))
        {

            if (!binary) //ASCII
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    end_body = true;
                    continue;
                }

                string[] tokens = line.Split();

                if (count < vert_count)
                {
                    //vert v = new vert(float.Parse(tokens[0]), float.Parse(tokens[1]), float.Parse(tokens[2]),
                    //    float.Parse(tokens[3]), float.Parse(tokens[4]), float.Parse(tokens[5]),
                    //    (char)int.Parse(tokens[6]), (char)int.Parse(tokens[7]), (char)int.Parse(tokens[8]));
                    // datavert.Add(v);
                    //if (count == header_count + vert_count)
                    //    Debug.Log(datavert.Count + ": " + line);

                    vertices[count] = new Vector3(float.Parse(tokens[ix]) * scale, float.Parse(tokens[iy]) * scale, float.Parse(tokens[iz]) * scale);
                    if(normals_present)
                        normals[count] = new Vector3(float.Parse(tokens[inx]), float.Parse(tokens[iny]), float.Parse(tokens[inz]));
                    if (colors_present)
                        colors[count] = new Color(int.Parse(tokens[ir]) / 255.0f, int.Parse(tokens[ig]) / 255.0f, int.Parse(tokens[ib]) / 255.0f, alpha_present ? int.Parse(tokens[ia]) / 255.0f : 1.0f);
                    indices[count] = count;
                    //colors[count - header_count - 1] = new Color(1, 0, 0,1);
                    //if (count == header_count + vert_count)
                    //{
                    //    Debug.Log(vertices[count - header_count - 1]);
                    //    Debug.Log("Last color " + colors[count - header_count - 1].ToString());
                    //}
                }
                else if (count < vert_count + tri_count)
                {
                    //datavert.Add(new vert(float.Parse(tokens[0]), float.Parse(tokens[1]), float.Parse(tokens[2]),
                    //    float.Parse(tokens[3]), float.Parse(tokens[4]), float.Parse(tokens[5]),
                    //    (char)int.Parse(tokens[6]), (char)int.Parse(tokens[7]), (char)int.Parse(tokens[8])));
                    //Debug.Log("Tri: " + line);
                    int idx = (count - vert_count) * 3;
                    triangles[idx] = int.Parse(tokens[1]);
                    triangles[idx + 1] = int.Parse(tokens[2]);
                    triangles[idx + 2] = int.Parse(tokens[3]);

                    //if (count == header_count + vert_count + tri_count)
                    //{
                    //    Debug.Log(triangles[count - header_count - vert_count - 1 + 2]);
                    //}
                }

                ++count;
            }
            else ///BINARY
            {
                //Debug.Log("Stream Pos " + BytesRead);
                ms.Position = BytesRead;

                long BytesToRead = vert_count * 4 * sizeof(float);
                if (floats.Length < vert_count * 4)
                {
                    floats = new float[(int)(vert_count * 4 * 1.2f)];
                    Debug.Log("NEW " + floats.Length);
                }

                Buffer.BlockCopy(buffer, (int)BytesRead, floats, 0, vert_count * 4 * sizeof(float));
                int color_offset = (int)BytesRead + 12;
                while (count < vert_count)
                {
                    vertices[count] = new Vector3(floats[count * 4 + 0] * scale, floats[count * 4 + 1] * scale, floats[count * 4 + 2] * scale);
                    colors[count] = new Color((int)buffer[color_offset] / 255.0f, (int)buffer[color_offset + 1] / 255.0f, (int)buffer[color_offset + 2] / 255.0f, (int)buffer[color_offset + 3] / 255.0f);
                    indices[count] = count;
                    //if (count < 10)
                    //{
                    //    Debug.Log("Vertex [" + vertices[count].x + "," + vertices[count].y + "," + vertices[count].z + "]" + " - [" + 
                    //        (int)(colors[count].r * 255.0f) + "," + (int)(colors[count].g*255.0f) + "," + (int)(colors[count].b * 255.0f) + "," + (int)(colors[count].a * 255.0f) + "]");
                    //}
                    color_offset += 16;
                    ++count;
                }
                end_body = true;
                //GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
            }
        }


        if (tri_count <= 0)
            renderAsPointCloud = true;

        if (vert_count > 0)
        {
            mesh.vertices = vertices;
            if (!renderAsPointCloud)
                mesh.triangles = triangles;
            if (normals_present)
                mesh.normals = normals;
            //mesh.uv = uvs;
            if (colors_present)
                mesh.colors = colors;
            if (renderAsPointCloud)
                mesh.SetIndices(indices, MeshTopology.Points, 0);

        }

        //Debug.Log("Loaded PLY " + PLY_file.ToString() + " (" + vert_count + " verts, " + tri_count + " faces) in " + (Time.realtimeSinceStartup - loadingTime) * 1000.0f + " ms");
        loading = false;

        //DestroyImmediate(mesh);
    }

    public bool isLoading()
    {
        return loading;
    }
}
