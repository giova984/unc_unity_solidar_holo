﻿Shader "Holo/PointCloudShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_PointSize("Point Size", Range(0,100.0)) = 1.0
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Geometry-1"}
		LOD 100

		//ZWrite On
		Cull Off ZWrite On //ZTest Always
	
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"

		Pass
		{
			CGPROGRAM
				#pragma target 5.0
				#pragma vertex vert
				//#pragma geometry geom
				#pragma geometry GS_POINT
				#pragma fragment frag
			
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					//float3 normal : NORMAL;
					//float2 uv : TEXCOORD0;
					fixed4 color : COLOR0;
				};

				struct v2f
				{
					float4 pos : POSITION;
					//float3 normal : NORMAL;
					//float2 uv : TEXCOORD0;
					//float3 worldPosition : TEXCOORD1;
					fixed4 color : COLOR0;
				};

				struct FS_INPUT
				{
					float4	pos		: POSITION;
					fixed4 color : COLOR0;
				};

				sampler2D _MainTex;
				//float4 _MainTex_ST;

				float     _PointSize;

				v2f vert(appdata v)
				{
					v2f o;
					//o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.pos = v.vertex;

					//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					//o.normal = v.normal;
					//o.worldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
					o.color = v.color;
					return o;
				}

				[maxvertexcount(6)]
				void geom(triangle v2f input[3], inout TriangleStream<v2f> OutputStream)
				{
					v2f test = (v2f)0;
					v2f test2 = (v2f)0;
					//float3 normal = normalize(cross(input[1].worldPosition.xyz - input[0].worldPosition.xyz, input[2].worldPosition.xyz - input[0].worldPosition.xyz));
					for (int i = 0; i < 3; i++)
					{
						//test.normal = normal;
						//o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
						test.pos = mul(UNITY_MATRIX_MVP, input[i].pos);
						//test.uv = input[i].uv;
						test.color = input[i].color;
						OutputStream.Append(test);
					}
					OutputStream.RestartStrip();
					for (int i = 0; i < 3; i++)
					{
						test2.pos = mul(UNITY_MATRIX_MVP, input[i].pos + float4(1.0f,0,0,0));
						//test2.pos.x += 1.0f;
						//test.uv = input[i].uv;
						test2.color = input[i].color;
						OutputStream.Append(test2);
					}
					OutputStream.RestartStrip();
				}

				[maxvertexcount(6)]
				void GS_POINT(point v2f input[1], inout TriangleStream<v2f> OutStream)
				{
					float size = (0.01f *_PointSize) / (_ScreenParams.x / 1268.0f);
					float ar = _ScreenParams.x / _ScreenParams.y;
					v2f Out[4];
					Out[0].pos = mul(UNITY_MATRIX_MVP, input[0].pos);
					Out[0].pos.x = Out[0].pos.x - size;
					Out[0].pos.y = Out[0].pos.y - size*ar;
					Out[0].color = input[0].color;

					Out[1].pos = mul(UNITY_MATRIX_MVP, input[0].pos);
					Out[1].pos.x = Out[1].pos.x - size;
					Out[1].pos.y = Out[1].pos.y + size*ar;
					Out[1].color = input[0].color;

					Out[2].pos = mul(UNITY_MATRIX_MVP, input[0].pos);
					Out[2].pos.x = Out[2].pos.x + size;
					Out[2].pos.y = Out[2].pos.y - size*ar;
					Out[2].color = input[0].color;

					Out[3].pos = mul(UNITY_MATRIX_MVP, input[0].pos);
					Out[3].pos.x = Out[3].pos.x + size;
					Out[3].pos.y = Out[3].pos.y + size*ar;
					Out[3].color = input[0].color;

					//int i;					

					//for (uint i = 0; i<4; ++i)
					//{
					//	//Out[i].pos = mul(Out[i].pos, UNITY_MATRIX_MVP);
					//	OutStream.Append(Out[i]);
					//}

					
					OutStream.Append(Out[0]);
					OutStream.Append(Out[1]);
					OutStream.Append(Out[2]);

					OutStream.RestartStrip();

					OutStream.Append(Out[1]);
					OutStream.Append(Out[2]);
					OutStream.Append(Out[3]);

					OutStream.RestartStrip();

				}

				fixed4 frag(v2f i) : SV_Target
				{
					//return fixed4(0.5,0,0,1);
					return i.color;
					// sample the texture
					/*fixed4 col = tex2D(_MainTex, i.uv);

					float3 lightDir = float3(1, 1, 0);
					float ndotl = dot(i.normal, normalize(lightDir));

					return col * ndotl;*/
				}
			ENDCG
		}
	}
}


//Shader "Unlit / PointCloudShader"
//{
//	Properties
//	{
//		_MainTex("TileTexture", 2D) = "white" {}
//		_PointSize("Point Size", Float) = 1.0
//	}
//
//		SubShader
//	{
//		LOD 200
//
//		Pass
//	{
//		CGPROGRAM
//
//#pragma only_renderers d3d11
//#pragma target 4.0
//
//#include "UnityCG.cginc"
//
//#pragma vertex   myVertexShader
//#pragma geometry myGeometryShader
//#pragma fragment myFragmentShader
//
//#define TAM 36
//
//		struct vIn // Into the vertex shader
//	{
//		float4 vertex : POSITION;
//		float4 color  : COLOR0;
//	};
//
//	struct gIn // OUT vertex shader, IN geometry shader
//	{
//		float4 pos : SV_POSITION;
//		float4 col : COLOR0;
//	};
//
//	struct v2f // OUT geometry shader, IN fragment shader 
//	{
//		float4 pos           : SV_POSITION;
//		//float2 uv_MainTex : TEXCOORD0;
//		float4 col : COLOR0;
//	};
//
//	float4       _MainTex_ST;
//	sampler2D _MainTex;
//	float     _PointSize;
//	// ----------------------------------------------------
//	gIn myVertexShader(vIn v)
//	{
//		gIn o; // Out here, into geometry shader
//			   // Passing on color to next shader (using .r/.g there as tile coordinate)
//		o.col = v.color;
//		// Passing on center vertex (tile to be built by geometry shader from it later)
//		o.pos = v.vertex;
//
//		return o;
//	}
//
//	// ----------------------------------------------------
//
//	[maxvertexcount(TAM)]
//	// ----------------------------------------------------
//	// Using "point" type as input, not "triangle"
//	void myGeometryShader(point gIn vert[1], inout TriangleStream<v2f> triStream)
//	{
//		float f = _PointSize / 40.0f; //half size
//
//		const float4 vc[TAM] = { float4(-f,  f,  f, 0.0f), float4(f,  f,  f, 0.0f), float4(f,  f, -f, 0.0f),    //Top                                 
//			float4(f,  f, -f, 0.0f), float4(-f,  f, -f, 0.0f), float4(-f,  f,  f, 0.0f),    //Top
//
//			float4(f,  f, -f, 0.0f), float4(f,  f,  f, 0.0f), float4(f, -f,  f, 0.0f),     //Right
//			float4(f, -f,  f, 0.0f), float4(f, -f, -f, 0.0f), float4(f,  f, -f, 0.0f),     //Right
//
//			float4(-f,  f, -f, 0.0f), float4(f,  f, -f, 0.0f), float4(f, -f, -f, 0.0f),     //Front
//			float4(f, -f, -f, 0.0f), float4(-f, -f, -f, 0.0f), float4(-f,  f, -f, 0.0f),     //Front
//
//			float4(-f, -f, -f, 0.0f), float4(f, -f, -f, 0.0f), float4(f, -f,  f, 0.0f),    //Bottom                                         
//			float4(f, -f,  f, 0.0f), float4(-f, -f,  f, 0.0f), float4(-f, -f, -f, 0.0f),     //Bottom
//
//			float4(-f,  f,  f, 0.0f), float4(-f,  f, -f, 0.0f), float4(-f, -f, -f, 0.0f),    //Left
//			float4(-f, -f, -f, 0.0f), float4(-f, -f,  f, 0.0f), float4(-f,  f,  f, 0.0f),    //Left
//
//			float4(-f,  f,  f, 0.0f), float4(-f, -f,  f, 0.0f), float4(f, -f,  f, 0.0f),    //Back
//			float4(f, -f,  f, 0.0f), float4(f,  f,  f, 0.0f), float4(-f,  f,  f, 0.0f)     //Back
//		};
//
//
//		const float2 UV1[TAM] = { float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),         //Esta em uma ordem
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),         //aleatoria qualquer.
//
//			float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//
//			float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//
//			float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//
//			float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//
//			float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
//			float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f)
//		};
//
//		const int TRI_STRIP[TAM] = { 0, 1, 2,  3, 4, 5,
//			6, 7, 8,  9,10,11,
//			12,13,14, 15,16,17,
//			18,19,20, 21,22,23,
//			24,25,26, 27,28,29,
//			30,31,32, 33,34,35
//		};
//
//		v2f v[TAM];
//		int i;
//
//		// Assign new vertices positions 
//		for (i = 0;i<TAM;i++) { v[i].pos = vert[0].pos + vc[i]; v[i].col = vert[0].col; }
//
//		// Assign UV values
//		//for (i = 0;i<TAM;i++) v[i].uv_MainTex = TRANSFORM_TEX(UV1[i],_MainTex);
//
//		// Position in view space
//		for (i = 0;i<TAM;i++) { v[i].pos = mul(UNITY_MATRIX_MVP, v[i].pos); }
//
//		// Build the cube tile by submitting triangle strip vertices
//		for (i = 0;i<TAM / 3;i++)
//		{
//			triStream.Append(v[TRI_STRIP[i * 3 + 0]]);
//			triStream.Append(v[TRI_STRIP[i * 3 + 1]]);
//			triStream.Append(v[TRI_STRIP[i * 3 + 2]]);
//
//			triStream.RestartStrip();
//		}
//	}
//
//	// ----------------------------------------------------
//	float4 myFragmentShader(v2f IN) : COLOR
//	{
//		//return float4(1.0,0.0,0.0,1.0);
//		return IN.col;
//	}
//
//		ENDCG
//	}
//	}
//}