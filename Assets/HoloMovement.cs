﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HoloMovement : NetworkBehaviour
{

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if ((isServer) || isLocalPlayer)
            return;

        Camera mainCamera = Camera.current;// GameObject.FindObjectOfType<Camera>();
        if (mainCamera)
        {
            //Camera camera = mainCamera.GetComponent<Camera>();
            Camera camera = mainCamera;
            camera.transform.position = this.transform.position;
            camera.transform.rotation = this.transform.rotation;
            camera.transform.Translate(new Vector3(0.1f, 0.1f, 0.1f));
            Debug.Log("Update main camera " + this.transform.position);
        }
        else
        {
            Debug.Log("Update NO main camera ");
        }
	}
}
