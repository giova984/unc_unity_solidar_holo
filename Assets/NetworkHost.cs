﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkHost : MonoBehaviour {

    public bool isServer = true;

	// Use this for initialization
	void Start () {
        NetworkManager nm = GetComponent<NetworkManager>();
        if (nm)
        {
            if(isServer)
                nm.StartHost();
            else
                nm.StartClient();
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
